unit PrimitivesLoader;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  TPackedGroup = Record
    name: String;
    position: Int64;
    length: Int64;
end;

type
  TPrimitivesLoader = class(TObject)
    primitives_file: TFileStream;
    packed_groups: array[1..256] of TPackedGroup;
    procedure LoadModel(aFileName: String);
  private
    // Private
    procedure get_primitives_info;
    procedure get_primitives_vertices(vertices_name: String);
  public
    // Public
  end;

implementation

uses MainForm;

procedure TPrimitivesLoader.LoadModel(aFileName: String);
var
  header: UInt32 = 0;
begin
  try
    primitives_file := TFileStream.Create(aFileName, fmOpenRead + fmShareDenyWrite);
    primitives_file.ReadBuffer(header, 4);
    if (header = $42A14E65) then
    begin
      get_primitives_info();
      get_primitives_vertices('vertices');
    end else
    begin
      MainWindow.log('File is not primitives', 'ERROR');
    end;
  finally
    if Assigned(primitives_file) then
      FreeAndNil(primitives_file);
  end;
end;

procedure TPrimitivesLoader.get_primitives_info;
var
  table_start: UInt32 = 0;
  section_size: UInt32;
  section_name_length: UInt32;
  position: Int64 = 4;
  section_name: String;
  i: Integer = 1;
begin
  primitives_file.Seek(-4, soFromEnd);
  primitives_file.ReadBuffer(table_start, 4);
  primitives_file.Seek(-4-table_start, soFromEnd);
  while (primitives_file.Size - primitives_file.Position) > 24 do
  begin
    section_size := 0;
    primitives_file.ReadBuffer(section_size, 4);

    primitives_file.Position := primitives_file.Position + 16;

    section_name_length := 0;
    primitives_file.ReadBuffer(section_name_length, 4);

    section_name := '';
    SetLength(section_name, section_name_length);
    primitives_file.ReadBuffer(section_name[1], section_name_length);

    packed_groups[i].name := section_name;
    packed_groups[i].position := position;
    packed_groups[i].length := section_size;

    Inc(i);

    position := position + section_size;
    if (section_size mod 4) > 0 then
      position := position + 4 - (section_size mod 4);

    if (section_name_length mod 4) > 0 then
      primitives_file.Position := primitives_file.Position + 4 - (section_name_length mod 4);
  end;
end;

procedure TPrimitivesLoader.get_primitives_vertices(vertices_name: String);
var
  i: Integer;
  idx: Integer = 0;
  vertices_subname,
  vertexFormat: String;
  verticesCount: UInt32 = 0;
begin
  for i:=1 to Length(packed_groups) do
    if packed_groups[i].name = vertices_name then
    begin
      idx := i;
      break;
    end;

  if idx > 0 then
  begin
    // packed_groups[i].position;
    // packed_groups[i].length;
    primitives_file.Seek(packed_groups[i].position, soFromBeginning);

    vertices_subname := '';
    SetLength(vertices_subname, 64);
    primitives_file.ReadBuffer(vertices_subname[1], 64);
    MainWindow.log('vertices_subname: ' + vertices_subname, 'INFO');

    if Copy(vertices_subname, 1, 4) = 'BPVT' then
    begin
      vertexFormat := '';
      SetLength(vertexFormat, 64);
      primitives_file.Position := primitives_file.Position + 4;
      primitives_file.ReadBuffer(vertexFormat[1], 64);
      MainWindow.log('vertexFormat: ' + vertexFormat, 'INFO');
    end;

    primitives_file.ReadBuffer(verticesCount, 4);

    if vertexFormat = 'set3/xyznuvtbpc' then
    begin
      // ToDo
    end;

  end;
end;

end.

