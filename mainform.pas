unit MainForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, OpenGLContext, Forms, Controls, Graphics,
  Dialogs, Menus, ExtCtrls, StdCtrls, GL, GLU, PrimitivesLoader;

type

  { TMainWindow }

  TMainWindow = class(TForm)
    MainMenu: TMainMenu;
    FileItem: TMenuItem;
    HelpItem: TMenuItem;
    AboutItem: TMenuItem;
    LogMemo: TMemo;
    ModelOpenDialog: TOpenDialog;
    OpenGLControl1: TOpenGLControl;
    OpenItem: TMenuItem;
    ExitItem: TMenuItem;
    Panel1: TPanel;
    LogPanel: TPanel;
    procedure AboutItemClick(Sender: TObject);
    procedure ExitItemClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OpenGLControl1Paint(Sender: TObject);
    procedure OpenItemClick(Sender: TObject);
    procedure log(log_text: String; log_context: String);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  MainWindow: TMainWindow;
  primitives_loader: TPrimitivesLoader;

implementation

{$R *.lfm}

{ TMainWindow }


procedure TMainWindow.AboutItemClick(Sender: TObject);
begin
  Application.MessageBox('BigWorld Model Viewer' + sLineBreak +
                         'Author: ShadowHunterRUS' + sLineBreak +
                         'Version from: 11.02.2016', 'About');
end;

procedure TMainWindow.ExitItemClick(Sender: TObject);
begin
  Close;
end;

procedure TMainWindow.FormCreate(Sender: TObject);
begin
  log('FormCreate', 'INFO');
end;

procedure TMainWindow.OpenGLControl1Paint(Sender: TObject);
begin
  glClearColor(1.0, 1.0, 1.0, 1.0);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, double(width) / height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glTranslatef(0.0, 0.0,-6.0);

  OpenGLControl1.SwapBuffers;
end;

procedure TMainWindow.OpenItemClick(Sender: TObject);
begin
  if ModelOpenDialog.Execute then
  begin
    log('FileName: ' + ModelOpenDialog.FileName, 'INFO');
    primitives_loader := TPrimitivesLoader.Create;
    primitives_loader.LoadModel(ModelOpenDialog.FileName);
    FreeAndNil(primitives_loader);
  end;
end;





procedure TMainWindow.log(log_text: String; log_context: String);
begin
  LogMemo.Lines.Add(Format('[%s]: %s', [log_context, log_text]) );
end;

end.

